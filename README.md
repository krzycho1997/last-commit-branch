﻿# Welcome to last-commit-branch!

This is a simple app for getting hash of last commit in branch. App was written thanks to PHP 8 and few simple composer packages.


## Requirements

 - Docker 
 - Some free space on your disk

## Installation

 1. Build image: 
 
	 `$ ./docker_build`
 
 2. Run container:
 
	 `$ ./docker_run`
	 
 3. Install composer:
 
	`$ ./container composer install`


## Usage

 - Simple run:

	  `$ ./app <space>/<repo> <branch>`
    
 - Run with optional service (GitHub default):

	  `$ ./app --service=<serivece name> <space>/<repo> <branch>`

 - Example:

	  `$ ./app --service=github krzycho1997/test-repo main`

## How to add own repo service?

 1. Create dir and class and put them into:

	`src/Service/RepoService/<YourNewDir>RepoService`

 2. Implement interface and write method body:
 
	`RepoServiceInterface`

 3. Add your class to:
	
    `config/repo_service_map`

	Where your new repo service name will be array key and element will be 	path to your class

## Tests

    $ ./test
