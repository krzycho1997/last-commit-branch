<?php

namespace Command;

use App\Command\LastCommitBranchHashCommand;
use App\Provider\RepoServiceProvider\RepoServiceProvider;
use App\Service\RepoService\GithubRepoService\GithubRepoService;
use Closure;
use Mockery;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;

class LastCommitBranchHashTest extends TestCase
{
    /**
     * Commit hash generated for testing purposes
     * @var string TEST_COMMIT_HASH
     */
    private const TEST_COMMIT_HASH = '2a69708797dff52a40fe388bc34efa935a7935bb';

    /**
     * Repo name for testing purposes
     * @var string TEST_REPO_NAME
     */
    private const TEST_REPO_NAME = 'krzycho1997/last-commit-branch';

    /**
     * Branch for testing purposes
     * @var string TEST_BRANCH
     */
    private const TEST_BRANCH = 'master';

    /**
     * Closure to choose repo service
     * @var Closure $repoServiceResolver
     */
    private Closure $repoServiceResolver;

    /**
     * Executing before tests
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->repoServiceResolver = (new RepoServiceProvider(
            $this->getRepoServicesMapWithMockedClasses()
        ))->getChooseRepoServiceClosure();
    }

    /**
     * Test user can get last commit hash after executing command with required parameters
     * @return void
     */
    public function testUserCanGetLastCommitHashAfterExecutingCommandWithRequiredParameters(): void
    {
        $commandTester = new CommandTester(
            new LastCommitBranchHashCommand($this->repoServiceResolver)
        );

        $commandTester->execute([
            'repo_name' => self::TEST_REPO_NAME,
            'branch' => self::TEST_BRANCH,
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString(self::TEST_COMMIT_HASH, $output);
    }

    /**
     * Test user can see exception when choose unknown service after executing command with required parameters
     * @return void
     */
    public function testUserCanSeeExceptionWhenChooseUnknownServiceAfterExecutingCommandWithRequiredParameters(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unknown service \'gitlab\'');

        $commandTester = new CommandTester(
            new LastCommitBranchHashCommand($this->repoServiceResolver)
        );

        $commandTester->execute([
            '--service' => 'gitlab',
            'repo_name' => self::TEST_REPO_NAME,
            'branch' => self::TEST_BRANCH,
        ]);
    }

    /**
     * Executing after tests
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();

        Mockery::close();
    }

    /**
     * Getting service map with mocked repo services for testing purposes
     * @return array
     */
    public function getRepoServicesMapWithMockedClasses(): array
    {
        return [
            'github' => $this->getMockedRepoService(
                GithubRepoService::class,
                self::TEST_REPO_NAME,
                self::TEST_BRANCH),
            'bitbucket' => $this->getMockedRepoService(
                BitbucketRepoService::class,
                self::TEST_REPO_NAME,
                self::TEST_BRANCH),
        ];
    }

    /**
     * Mocking repos services
     * @param string $repoServiceClass
     * @param string $repoName
     * @param string $branch
     * @return mixed
     */
    private function getMockedRepoService(string $repoServiceClass, string $repoName, string $branch): mixed
    {
        return Mockery
            ::mock($repoServiceClass)
            ->allows()
            ->getHash($repoName, $branch)
            ->andReturn(self::TEST_COMMIT_HASH)
            ->getMock();
    }
}