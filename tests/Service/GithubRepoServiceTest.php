<?php

namespace Service;

use App\Service\RepoService\GithubRepoService\GithubRepoService;
use PHPUnit\Framework\TestCase;

/**
 * Class GithubRepoServiceTest
 * @package Service
 */
class GithubRepoServiceTest extends TestCase
{
    /**
     * Commit hash generated for testing purposes
     * @var string TEST_COMMIT_HASH
     */
    private const TEST_COMMIT_HASH = 'a78ee271f709dfa000748efb655cfbefe42e2fa7';

    /**
     * Repo name for testing purposes
     * @var string TEST_REPO_NAME
     */
    private const TEST_REPO_NAME = 'krzycho1997/test-repo';

    /**
     * Branch for testing purposes
     * @var string TEST_BRANCH
     */
    private const TEST_BRANCH = 'main';

    /**
     * Instance of GithubRepoService for testing purposes
     * @var GithubRepoService $githubRepoService
     */
    private GithubRepoService $githubRepoService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->githubRepoService = new GithubRepoService();
    }

    /**
     * Test method can return last commit branch hash after executing with required parameters
     * @return void
     */
    public function testMethodCanReturnLastCommitBranchHashAfterExecutingWithRequiredParameters(): void
    {
        $hash = $this
            ->githubRepoService
            ->getHash(
                self::TEST_REPO_NAME,
                self::TEST_BRANCH
            )
        ;

        $this->assertStringContainsString(self::TEST_COMMIT_HASH, $hash);
    }
}