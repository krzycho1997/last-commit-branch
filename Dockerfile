FROM php:8.0.3-cli-alpine3.13

RUN apk update && apk upgrade && \
    apk add --no-cache git

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer