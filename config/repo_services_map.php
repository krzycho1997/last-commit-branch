<?php

use App\Service\RepoService\GithubRepoService\GithubRepoService;

return [
    'github' => GithubRepoService::class
];