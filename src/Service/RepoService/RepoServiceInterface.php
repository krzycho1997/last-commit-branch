<?php


namespace App\Service\RepoService;

/**
 * Interface RepoServiceInterface
 * @package App\Service\RepoService
 */
interface RepoServiceInterface
{
    /**
     * Getting hash of last commit branch method declaration
     * @param string $repoName
     * @param string $branch
     * @return string
     */
    public function getHash(string $repoName, string $branch): string;
}