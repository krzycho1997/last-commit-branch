<?php


namespace App\Service\RepoService\GithubRepoService;


use App\Service\RepoService\RepoServiceInterface;

/**
 * Class GithubRepoService
 * @package App\Service\RepoService\GithubRepoService
 */
class GithubRepoService extends AbstractGithubRepoService implements RepoServiceInterface
{
    /**
     * Getting last commit hash of branch
     * @param string $repoName
     * @param string $branch
     * @return string
     */
    public function getHash(string $repoName, string $branch): string
    {
        return $this->getTrimmedResultOfCommand(
            self::GITHUB_URL,
            $repoName,
            $branch
        );
    }
}