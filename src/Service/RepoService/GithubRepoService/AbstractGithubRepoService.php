<?php


namespace App\Service\RepoService\GithubRepoService;


use App\Service\RepoService\AbstractHelperRepoService;

/**
 * Class AbstractGithubRepoService
 * @package App\Service\RepoService\GithubRepoService
 */
abstract class AbstractGithubRepoService extends AbstractHelperRepoService
{
    /**
     * Github repo service URL
     * @var string GITHUB_URL
     */
    protected const GITHUB_URL = 'https://github.com/';
}