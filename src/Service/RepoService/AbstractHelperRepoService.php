<?php


namespace App\Service\RepoService;


use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class AbstractHelperRepoService
 * @package App\Service\RepoService
 */
class AbstractHelperRepoService
{
    /**
     * Getting built command based on 'git ls-remote' and 'awk'
     * @param $repoServiceUrl
     * @param $repoName
     * @param $branch
     * @return string
     */
    private function getBuiltCommand($repoServiceUrl, $repoName, $branch): string
    {
        return 'git ls-remote '
            . $repoServiceUrl
            . $repoName
            . '.git'
            . ' '
            . $branch
            . ' | awk \'{print $1}\'';
    }

    /**
     * Executing command and returning result
     * @param $command
     * @return string
     */
    private function getResultOfCommand($command): string
    {
        $process = Process::fromShellCommandline($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    /**
     * Calling to executing method and returning trimmed result
     * @param $repoServiceUrl
     * @param $repoName
     * @param $branch
     * @return string
     */
    protected function getTrimmedResultOfCommand($repoServiceUrl, $repoName, $branch): string
    {
        $builtCommand = $this->getBuiltCommand($repoServiceUrl, $repoName, $branch);
        $resultOfCommand = $this->getResultOfCommand($builtCommand);

        return trim($resultOfCommand);
    }
}