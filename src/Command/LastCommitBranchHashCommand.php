<?php

namespace App\Command;

use App\Service\RepoService\RepoServiceInterface;
use Closure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LastCommitBranchHashCommand
 * @package App\Command
 */
class LastCommitBranchHashCommand extends Command
{
    /**
     * Command name for executing
     * @var string $defaultName
     */
    protected static $defaultName = 'app:get-last-commit-branch-hash';

    /**
     * Instance of chosen repo service
     * @var RepoServiceInterface $repoServiceInstance
     */
    private RepoServiceInterface $repoServiceInstance;

    /**
     * LastCommitBranchHashCommand constructor.
     * @param Closure $repoServiceResolver
     * @param string|null $name
     */
    public function __construct(
        private Closure $repoServiceResolver,
        string $name = null
    )
    {
        parent::__construct($name);
    }

    /**
     * Command configuring
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addOption(
                name: 'service',
                mode: InputOption::VALUE_OPTIONAL,
                description: 'Kind of repo provider',
                default: 'github',
            )
            ->addArgument('repo_name', InputArgument::REQUIRED, 'The name of repo.')
            ->addArgument('branch', InputArgument::REQUIRED, 'The name of branch.')
        ;
    }

    /**
     * Command executing
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->repoServiceInstance = call_user_func(
            $this->repoServiceResolver,
            $input->getOption('service')
        );

        $hash = $this->repoServiceInstance->getHash(
            $input->getArgument('repo_name'),
            $input->getArgument('branch')
        );

        $output->writeln($hash);

        return Command::SUCCESS;
    }
}