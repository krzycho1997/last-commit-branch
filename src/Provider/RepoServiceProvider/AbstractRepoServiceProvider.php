<?php


namespace App\Provider\RepoServiceProvider;


use App\Command\LastCommitBranchHashCommand;
use Closure;
use RuntimeException;

/**
 * Class AbstractRepoServiceProvider
 * @package App\Provider\RepoServiceProvider
 */
abstract class AbstractRepoServiceProvider
{
    /**
     * Service map array for handling services
     * @var array
     */
    public array $repoServicesMap;

    /**
     * Getting closure for choose repo service from services repo array
     * @return Closure
     */
    public function getChooseRepoServiceClosure(): Closure
    {
        return function ($repoServiceName) {
            if (!array_key_exists($repoServiceName, $this->repoServicesMap)) {
                throw new RuntimeException(
                    "Unknown service '{$repoServiceName}'"
                );
            }
            return $this->repoServicesMap[$repoServiceName];
        };
    }

    /**
     * Getting closure for chosen repo service instance from services repo array
     * @return Closure
     */
    public function getChooseRepoServiceInstanceClosure(): Closure
    {
        return fn ($repoServiceName) =>
            new ($this->getChooseRepoServiceClosure()($repoServiceName));
    }

    /**
     * Getting closure with command new instance
     * @return Closure
     */
    protected function getLastCommitBranchHashCommandClosure(): Closure
    {
        return fn () => new LastCommitBranchHashCommand(
                $this->getChooseRepoServiceInstanceClosure()
        );
    }
}