<?php


namespace App\Provider\RepoServiceProvider;


use App\Command\LastCommitBranchHashCommand;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class RepoServiceProvider
 * @package App\Provider\RepoServiceProvider
 */
class RepoServiceProvider extends AbstractRepoServiceProvider implements ServiceProviderInterface
{
    /**
     * RepoServiceProvider constructor.
     * @param array $repoServicesMap
     */
    public function __construct(array $repoServicesMap)
    {
        $this->repoServicesMap = $repoServicesMap;
    }

    /**
     * Register command closure in app container
     * @param Container $pimple
     */
    public function register(Container $pimple): void
    {
        $pimple[LastCommitBranchHashCommand::class] =
            $this->getLastCommitBranchHashCommandClosure();
    }
}